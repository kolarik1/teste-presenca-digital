var app = angular.module('myApp', []);
app.controller('produtos', function($scope, $http) {
    $http.get("json/produtos.json").then(function(response) {
        $scope.produtos = response.data;
        currency($scope.produtos);
    });
});

function currency(produtos) {

    for (i = 0; i < produtos.length; i++) {
        var element = produtos[i];
        var preco = element.preco;

        element.preco = virgula(preco);
        var adesao = element.adesao;

        element.adesao = virgula(adesao);
        var taxaInstalacao = element.taxaInstalacao;

        element.taxaInstalacao = virgula(taxaInstalacao);
        var precoDe = element.precoDe;

        element.precoDe = virgula(precoDe);

    }
}

function virgula(int) {
    var tmp = int + '';
    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
    if (tmp.length > 6) {
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
    }
    return tmp;
}